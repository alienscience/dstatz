

// Setup chart
const colours = ['#e41a1c', '#377eb8', '#4daf4a', '#984ea3', '#ff7f00', '#ffff33', '#a65628', '#f781bf'];
var styleIdx = 0;

function chartDef(title) {
    return {
        type: 'line',

        data: {
            datasets: []
        },
        options: {
            title: {
                display: true,
                text: title
            },
            scales: {
                xAxes: [{
                    type: 'time',
                    distribution: 'linear',
                    time: {
                        displayFormats: {
                            second: 'HH:mm:ss'
                        }
                    },
                    ticks: {
                        source: 'auto'
                    }
                }],
                yAxes: [{
                    ticks: {
                        beginAtZero: true,
                        suggestedMax: 0
                    }
                }]
            }
        }
    };
}

function onLoad() {
    let charts = new Map();

    // Setup websocket
    let active = document.getElementById("active");
    let passive = document.getElementById("passive");
    let protocol = (location.protocol === 'https:') ? "wss://" : "ws://";
    let conn = new WebSocket(protocol + location.host + location.pathname + "/ws" + location.search);
    conn.onclose = function(evt) {
        active.style.display = "none";
        passive.style.display = "inline";
    };
    conn.onmessage = function(evt) {
        active.style.display = "inline";
        passive.style.display = "none";
        console.log("----\n" + evt.data);
        handleUpdate(charts, evt.data);
    };
}

// Handle updated metrics
function handleUpdate(charts, update) {
    let date = moment();
    let graph = "";
    let metric = "unknown";
    let maxY = 0;
    let lines = update.split("\n");
    let chart = null;
    lines.forEach(function(line) {
        let fields = line.split('|');
        switch(fields[0]) {
        case '@': // Date
            date = moment.utc(fields[1]);
            break;
        case '+': // Increment Date
            date = date.add(Number(fields[1]), 's');
            break;
        case 'g': // Graph
            graph = fields[1];
            chart = getChart(charts, graph);
            break;
        case 'm': // Metric
            metric = fields[1];
            v = fields[2];
            if (v > maxY) {
                maxY = Math.round(v);
            }
            if (chart !== null) {
                updateCounter(chart, metric, date.toDate(), v);
            }
            break;
        }
    });
    if (chart === null) {
        return;
    }
    let element = chart.element;
    let currentMax = element.options.scales.yAxes[0].ticks.suggestedMax;
    if (maxY > currentMax) {
        // Grow the yAxis to 30% bigger than max
        niceMax = Math.ceil(maxY) + Math.ceil(0.3 * maxY);
        element.options.scales.yAxes[0].ticks.suggestedMax = niceMax;
        element.update();
    }
}

function getChart(charts, id) {
    if (charts.has(id)) {
        console.log("Already have chart ", id);
        let chart = charts.get(id);
        console.log(chart);
        return chart;
    }
    // Create new chart
    let container = document.getElementById('container');
    let canvas = document.createElement('canvas');
    let div = document.createElement('div');
    let canvasId = `chart-${id}`;
    canvas.id = canvasId;
    div.className = 'chart';
    div.appendChild(canvas);
    container.appendChild(div);
    let ctx = document.getElementById(canvasId);
    console.log("Ctx ", ctx);
    let chart = {
        knownDatasets: new Map(),
        element: new Chart(ctx, chartDef(id))
    };
    charts.set(id, chart);
    console.log("New chart with id " + id, chart);
    return chart;
}


function updateCounter(chart, dataset, date, value) {
    console.log("Processing chart", chart);
    let dataPoint = {
        t: date,
        y: value
    };
    let knownDatasets = chart.knownDatasets;
    if (!knownDatasets.has(dataset)) {
        let colour = colours[styleIdx % colours.length];
        styleIdx += 1;
        let backgroundColour = Chart.helpers.color(colour).alpha(0.2).rgbString();
        knownDatasets.set(dataset, knownDatasets.size);
        let element = chart.element;
        element.data.datasets.push({
            label: dataset,
            backgroundColor: backgroundColour,
            borderColor: colour,
            radius: 1,
            fill: true,
            borderwidth: 1
        });
        element.update();
    } else {
        let i = knownDatasets.get(dataset);
        let element = chart.element;
        if (element.data.datasets[i].data.length >= 16) {
            element.data.datasets[i].data.shift();
        }
        element.data.datasets[i].data.push(dataPoint);
        element.update();
    }
}

