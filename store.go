package main

import (
	"gitlab.com/alienscience/dstatz/statsd"
	"sync"
	"time"
)

// The Store interface describes the requirements for a package that
// stores statistics.
type Store interface {
	Add(view *statsd.View, zoom int) error
	View(prefix string, from time.Time, count int, zoom int) (*statsd.View, error)
	Shutdown()
}

type listeningStore struct {
	statsd *statsd.Daemon
	store  Store
	wait   sync.WaitGroup
}

func newListeningStore(statsd *statsd.Daemon, store Store) *listeningStore {
	ret := new(listeningStore)
	ret.statsd = statsd
	ret.store = store
	return ret
}

func (l *listeningStore) listen(period time.Duration, zoom int) {
	l.wait.Add(1)
	go func() {
		defer l.wait.Done()
		handle, updateCh := l.statsd.Subscribe("", period)
		defer l.statsd.Unsubscribe(handle)
		for view := range updateCh {
			l.store.Add(view, zoom)
		}
	}()
}

func (l *listeningStore) shutdown() {
	// Wait for listener to finish
	l.wait.Wait()
	// Shutdown the store
	l.store.Shutdown()
}
