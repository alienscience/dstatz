#!/bin/bash

trap "kill 0" EXIT

# Go compile and restart on changes
export DEV="true"
gin --all -x db &

# Send dummy stats over UDP
while :
do
	g=$(($RANDOM / 1000))
	h=$(($RANDOM / 1000))
	i=$(($RANDOM / 1000))
	a=$(($RANDOM / 1000))
	b=$(($RANDOM / 1000))
	de=$(($RANDOM / 1000))
	ef=$(($RANDOM / 1000))
  cat <<EOF |  nc -w1 -u localhost 8125
abc.def.g:${g}|c
abc.def.h:${h}|c
abc.def.i:${i}|c
abc.fgh.a:${a}|c
abc.fgh.b:${b}|c
abc.ijk.de:${de}|c
abc.ijk.ef:${ef}|c
EOF
done

wait
