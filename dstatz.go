package main

import (
	"github.com/namsral/flag"
	"gitlab.com/alienscience/dstatz/badger"
	"gitlab.com/alienscience/dstatz/statsd"
	"log"
	"os"
	"os/signal"
	"syscall"
	"time"
)

const (
	internalStats = "dstatz."
	statsdStats   = internalStats + "statsd."
)

var (
	udpAddress    = flag.String("udp", ":8125", "UDP stats address")
	tcpAddress    = flag.String("tcp", ":8126", "TCP stats address")
	httpAddress   = flag.String("http", ":3000", "HTTP viewer address")
	ticksPerGraph = flag.Int("ticks-per-graph", 16, "Number of ticks in a graph")
	dbDir         = flag.String("db", "db", "Database directory")
	devMode       = flag.Bool("dev", false, "Development mode")
	// Needed for compatibility with gin during development
	httpPort = flag.String("port", "", "http port to listen on (dev mode)")
)

var (
	zoomLevels = []time.Duration{2 * time.Second, 30 * time.Second, 10 * time.Minute, 1 * time.Hour, 6 * time.Hour}
)

func runStatsd() (*statsd.Daemon, error) {
	statsd, err := statsd.NewDaemon(
		statsd.UDPAddress(*udpAddress),
		statsd.TCPAddress(*tcpAddress),
		statsd.TickPeriod(zoomLevels[0]),
		statsd.InternalStats(statsdStats),
	)
	if err != nil {
		return nil, err
	}
	err = statsd.Run()
	if err != nil {
		return nil, err
	}
	return statsd, nil
}

func runViewer(statsd *statsd.Daemon, store Store) (*viewer, error) {
	httpAddr := *httpAddress
	if *devMode {
		httpAddr = ":" + *httpPort
	}

	viewer := &viewer{
		statsd:        statsd,
		store:         store,
		httpAddress:   httpAddr,
		zoomLevels:    zoomLevels,
		ticksPerGraph: *ticksPerGraph,
	}
	go viewer.run()
	return viewer, nil
}

func runDb(statsd *statsd.Daemon) (*listeningStore, error) {
	badger, err := badger.NewStore(*dbDir)
	if err != nil {
		return nil, err
	}
	store := newListeningStore(statsd, badger)
	// Add subscriptions for each zoom level
	for zoom, period := range zoomLevels {
		store.listen(period, zoom)
	}
	return store, nil
}

func main() {
	flag.Parse()
	statsd, err := runStatsd()
	if err != nil {
		log.Fatal(err)
	}

	db, err := runDb(statsd)
	if err != nil {
		log.Fatal(err)
	}

	// statsd should be shutdown before the db
	defer db.shutdown()
	defer statsd.Shutdown()

	_, err = runViewer(statsd, db.store)
	if err != nil {
		log.Fatal(err)
	}

	signalChan := make(chan os.Signal, 1)
	signal.Notify(signalChan, syscall.SIGTERM, syscall.SIGINT)

	// Wait for a terminate signal
	sig := <-signalChan
	log.Printf("Got signal %v", sig)
}
