package main

import (
	"bytes"
	"context"
	"github.com/gorilla/mux"
	"gitlab.com/alienscience/dstatz/statsd"
	"log"
	"net/http"
	"net/url"
	"nhooyr.io/websocket"
	"strconv"
	"time"
)

const (
	viewerStats = internalStats + "viewer."
)

type viewer struct {
	statsd        *statsd.Daemon
	store         Store
	httpAddress   string
	zoomLevels    []time.Duration
	ticksPerGraph int
}

func (v *viewer) run() {
	r := mux.NewRouter()
	r.HandleFunc("/", v.serveHome).Methods("GET")
	v.serveStatic(r, "./static", "/static/")
	r.HandleFunc("/{prefix}/ws", v.serveWs).Methods("GET")
	r.HandleFunc("/{prefix}", v.serveGraphs).Methods("GET")

	log.Printf("HTTP listening on %s", v.httpAddress)
	if err := http.ListenAndServe(v.httpAddress, r); err != nil {
		log.Fatal(err)
	}
}

func (v *viewer) serveHome(w http.ResponseWriter, r *http.Request) {
	v.statsd.IncCounter(viewerStats+"homepage", 1)
	http.ServeFile(w, r, "static/homepage.html")
}

func (v *viewer) serveStatic(r *mux.Router, dir string, httpPath string) {
	r.PathPrefix(httpPath).
		Handler(http.StripPrefix(httpPath, http.FileServer(http.Dir(dir))))
}

func (v *viewer) serveWs(w http.ResponseWriter, r *http.Request) {
	v.statsd.IncCounter(viewerStats+"ws", 1)
	vars := mux.Vars(r)
	prefix := vars["prefix"]
	query := r.URL.Query()
	from, zoom, liveUpdates := v.setupZoom(query)
	log.Printf("From=%v, Zoom = %d, LiveUpdates=%v", from, zoom, liveUpdates)
	ws, err := websocket.Accept(w, r, &websocket.AcceptOptions{})
	if err != nil {
		log.Print(err)
		return
	}
	defer ws.Close(websocket.StatusNormalClosure, "Goodbye")
	ctx := r.Context()
	// Send history down websocket
	if err := v.writeHistory(ctx, ws, prefix, from, zoom); err != nil {
		return
	}
	if liveUpdates {
		v.statsd.IncCounter(viewerStats+"live", 1)
		v.statsUpdates(ctx, ws, prefix, zoom)
	}
}

func (v *viewer) serveGraphs(w http.ResponseWriter, r *http.Request) {
	w.Header().Set("Content-Type", "text/html; charset=utf-8")
	http.ServeFile(w, r, "static/graphs.html")
}

func (v *viewer) writeHistory(ctx context.Context, ws *websocket.Conn, prefix string, from time.Time, zoom int) error {
	view, err := v.store.View(prefix, from, v.ticksPerGraph, zoom)
	if err != nil {
		return err
	}
	return writeMessage(ctx, ws, view.ToBuffer())
}

func (v *viewer) statsUpdates(ctx context.Context, ws *websocket.Conn, prefix string, zoom int) {

	// Subscribe for updates
	handle, updates := v.statsd.Subscribe(prefix, v.zoomLevels[zoom])
	defer v.statsd.Unsubscribe(handle)

	for view := range updates {
		v.statsd.IncCounter(viewerStats+"updates", 1)
		if err := writeMessage(ctx, ws, view.ToBuffer()); err != nil {
			return // Websocket closed
		}
	}
}
func (v *viewer) setupZoom(query url.Values) (time.Time, int, bool) {
	from, hasFrom := timeFromQuery(query, "from")
	zoom, hasZoom := v.zoomFromQuery(query, "zoom")
	if !hasFrom && !hasZoom {
		return time.Now().Add(-time.Duration(v.ticksPerGraph) * v.zoomLevels[0]).UTC(), 0, true
	}
	if hasFrom && !hasZoom {
		return from, v.defaultZoom(from), true
	}
	// hasZoom is set
	earliestLiveUpdates := time.Now().Add(-time.Duration(v.ticksPerGraph) * v.zoomLevels[zoom]).UTC()
	if !hasFrom && hasZoom {
		return earliestLiveUpdates, zoom, true
	}
	// hasFrom && hasZoom
	liveUpdates := earliestLiveUpdates.Before(from)
	return from, zoom, liveUpdates
}

func (v *viewer) zoomFromQuery(query url.Values, key string) (int, bool) {
	val, ok := query[key]
	if !ok || len(val) == 0 {
		return 0, false
	}
	i, err := strconv.Atoi(val[0])
	if err != nil || i < 0 || i >= len(v.zoomLevels) {
		return 0, false
	}
	return i, true
}

func (v *viewer) defaultZoom(from time.Time) int {
	perfectTick := time.Duration(time.Now().Sub(from).Seconds()/float64(v.ticksPerGraph)) * time.Second
	for i, d := range v.zoomLevels {
		if d >= perfectTick {
			return i
		}
	}
	return len(v.zoomLevels) - 1
}

func writeMessage(ctx context.Context, ws *websocket.Conn, buf *bytes.Buffer) error {
	writer, err := ws.Writer(ctx, websocket.MessageText)
	if err != nil {
		return err
	}
	defer writer.Close()
	_, err = writer.Write(buf.Bytes())
	return err
}

func timeFromQuery(query url.Values, key string) (time.Time, bool) {
	v, ok := query[key]
	if !ok || len(v) == 0 {
		return time.Now(), false
	}
	t, err := time.Parse(statsd.TimeFormat, v[0])
	if err != nil {
		return time.Now(), false
	}
	return t, true
}
