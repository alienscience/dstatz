package badger

import (
	"bytes"
	"encoding/gob"
	"errors"
	"fmt"
	"github.com/dgraph-io/badger"
	"gitlab.com/alienscience/dstatz/statsd"
	"log"
	"strconv"
	"strings"
	"time"
)

type addRequest struct {
	view *statsd.View
	zoom int
}

type BadgerStore struct {
	db           *badger.DB
	ttl          time.Duration
	shutdownCh   chan bool
	isShutdownCh chan bool
}

func NewStore(dbDir string) (*BadgerStore, error) {
	options := badger.DefaultOptions(dbDir)
	options.SyncWrites = true
	db, err := badger.Open(options)
	if err != nil {
		return nil, err
	}
	ret := &BadgerStore{
		db:           db,
		ttl:          168 * time.Hour, // 1 Week
		shutdownCh:   make(chan bool),
		isShutdownCh: make(chan bool),
	}
	go ret.run()
	return ret, nil
}

func (s *BadgerStore) run() {
	defer s.db.Close()
	defer log.Print("Shutting down db")
	s.gc() // Run a GC on startup
	ticker := time.NewTicker(10 * time.Minute)
	defer ticker.Stop()
eventLoop:
	for {
		select {
		case <-ticker.C:
			s.gc()
		case <-s.shutdownCh:
			break eventLoop
		}
	}
	s.isShutdownCh <- true
}

func (s *BadgerStore) gc() {
	if err := s.db.RunValueLogGC(0.5); err != nil {
		// Some GC errors are not real errors - log but do not exit
		log.Print(err)
	}
}

func (s *BadgerStore) Shutdown() {
	// Wait for shutdown
	s.shutdownCh <- true
	<-s.isShutdownCh
}

func (s *BadgerStore) Add(view *statsd.View, zoom int) error {
	return s.db.Update(func(txn *badger.Txn) error {
		for i, t := range view.Times {
			timeKey := keyBytes(zoom, t)
			graphsValue := graphBytes(view.Graphs[i])
			expires := uint64(t.Add(s.ttl).Unix())
			entry := &badger.Entry{Key: timeKey, Value: graphsValue, ExpiresAt: expires}
			if err := txn.SetEntry(entry); err != nil {
				return err
			}
		}
		return nil
	})
}

func keyBytes(zoom int, t time.Time) []byte {
	timeStr := t.Format(statsd.TimeFormat)
	keyStr := fmt.Sprintf("%d:%s", zoom, timeStr)
	return []byte(keyStr)
}

func decodeKey(b []byte) (int, time.Time, error) {
	s := strings.SplitN(string(b), ":", 2)
	if len(s) != 2 {
		return 0, time.Now(), errors.New("DB key in unknown format")
	}
	zoom, err := strconv.Atoi(s[0])
	if err != nil {
		return 0, time.Now(), err
	}
	t, err := time.Parse(statsd.TimeFormat, s[1])
	return zoom, t, err
}

func graphBytes(g statsd.Graph) []byte {
	buffer := new(bytes.Buffer)
	enc := gob.NewEncoder(buffer)
	enc.Encode(g)
	return buffer.Bytes()
}

func graphFromBytes(b []byte) (map[string]statsd.Metric, error) {
	buffer := bytes.NewBuffer(b)
	dec := gob.NewDecoder(buffer)
	graph := make(map[string]statsd.Metric)
	err := dec.Decode(&graph)
	return graph, err
}

func (s *BadgerStore) View(prefix string, from time.Time, count int, zoom int) (*statsd.View, error) {
	ret := statsd.EmptyView()
	err := s.db.View(func(txn *badger.Txn) error {
		it := txn.NewIterator(badger.DefaultIteratorOptions)
		defer it.Close()
		fromBytes := keyBytes(zoom, from)
		i := 0
		for it.Seek(fromBytes); it.Valid(); it.Next() {
			item := it.Item()
			key := item.Key()
			i += 1
			if i > count {
				break
			}
			z, t, err := decodeKey(key)
			if err != nil {
				return err
			}
			if z != zoom {
				break
			}

			val, err := item.ValueCopy(nil)
			if err != nil {
				return err
			}
			graph, err := graphFromBytes(val)
			if err != nil {
				return err
			}
			filtered := filterByPrefix(prefix, graph)
			if len(filtered) > 0 {
				ret.Times = append(ret.Times, t)
				ret.Graphs = append(ret.Graphs, filtered)
			}
		}
		return nil
	})
	return ret, err
}

func filterByPrefix(prefix string, graph map[string]statsd.Metric) map[string]statsd.Metric {
	ret := make(map[string]statsd.Metric)
	for k, v := range graph {
		if strings.HasPrefix(k, prefix) {
			name := strings.TrimPrefix(k, prefix)
			ret[name] = v
		}
	}
	return ret
}
