package statsd

import (
	"sync"
	"time"
)

type subscription struct {
	stats      *stats
	prefix     string
	period     time.Duration
	ch         chan *View
	lastUpdate time.Time
}

type pubSub struct {
	idCounter     uint32
	subscriptions map[uint32]*subscription
	mutex         sync.Mutex
}

func newPubSub() *pubSub {
	return &pubSub{
		idCounter:     0,
		subscriptions: make(map[uint32]*subscription),
	}
}

func (p *pubSub) subscribe(prefix string, period time.Duration) (uint32, chan *View) {
	p.mutex.Lock()
	defer p.mutex.Unlock()
	p.idCounter++
	id := p.idCounter
	subscription := &subscription{
		stats:      newStats(),
		prefix:     prefix,
		period:     period,
		ch:         make(chan *View),
		lastUpdate: time.Now(),
	}
	p.subscriptions[id] = subscription
	return id, subscription.ch
}

func (p *pubSub) unsubscribe(id uint32) {
	p.mutex.Lock()
	defer p.mutex.Unlock()
	delete(p.subscriptions, id)
}

func (p *pubSub) numSubscriptions() int {
	return len(p.subscriptions)
}

func (p *pubSub) update(update *stats, date time.Time) {
	p.mutex.Lock()
	defer p.mutex.Unlock()
	for _, sub := range p.subscriptions {
		sub.stats.update(update, sub.prefix)
		timeDiff := date.Sub(sub.lastUpdate)
		if timeDiff >= sub.period {
			view := fromStats(sub.stats, date, timeDiff)
			sendView(view, sub.ch)
			sub.stats.restart()
			sub.lastUpdate = date
		}
	}
}

func (p *pubSub) flush(update *stats, date time.Time) {
	p.mutex.Lock()
	defer p.mutex.Unlock()
	for _, sub := range p.subscriptions {
		sub.stats.update(update, sub.prefix)
		timeDiff := date.Sub(sub.lastUpdate)
		view := fromStats(sub.stats, date, timeDiff)
		sendView(view, sub.ch)
		sub.stats.restart()
		sub.lastUpdate = date
	}
}

func (p *pubSub) shutdown() {
	p.mutex.Lock()
	defer p.mutex.Unlock()
	for _, sub := range p.subscriptions {
		close(sub.ch)
	}
}

func sendView(view *View, ch chan<- *View) {
	if !view.Empty() {
		select {
		case ch <- view: // Non blocking send to channel
		default:
		}
	}
}
