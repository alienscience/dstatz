package statsd

import (
	"bytes"
	"fmt"
	"math"
	"sort"
	"strings"
	"time"
)

const (
	// TimeFormat is the time format written by byte buffers
	TimeFormat = "20060102T150405"
)

// View is a view of statistics over different times
type View struct {
	Times  []time.Time
	Graphs []Graph
}

// Graph defines multiple graphs
type Graph map[string]Metric

// Metric defines multiple metrics values on a single graph
type Metric map[string]float64

// EmptyView creates an empty view
func EmptyView() *View {
	return &View{
		Times:  make([]time.Time, 0, 8),
		Graphs: make([]Graph, 0, 8),
	}
}

// Empty returns true if the view is empty
func (v *View) Empty() bool {
	return len(v.Times) == 0
}

// ToBuffer writes a view to a bytes.Buffer in a human readable format with column
// compression.
func (v *View) ToBuffer() *bytes.Buffer {
	buffer := new(bytes.Buffer)
	for i, t := range v.Times {
		if i == 0 {
			fmt.Fprintf(buffer, "@|%s\n", t.Format(TimeFormat))
		} else {
			diff := math.Round(t.Sub(v.Times[i-1]).Seconds())
			fmt.Fprintf(buffer, "+|%d\n", uint32(diff))
		}
		// Output graphs in sorted order
		graphs := make([]string, 0, len(v.Graphs[i]))
		for g := range v.Graphs[i] {
			graphs = append(graphs, g)
		}
		sort.Strings(graphs)
		for _, g := range graphs {
			metricMap := v.Graphs[i][g]
			fmt.Fprintf(buffer, "g|%s\n", g)
			// Output metrics in sorted order
			metrics := make([]string, 0, len(metricMap))
			for m := range metricMap {
				metrics = append(metrics, m)
			}
			sort.Strings(metrics)
			for _, m := range metrics {
				fmt.Fprintf(buffer, "m|%s|%.3g\n", m, metricMap[m])
			}
		}
	}
	return buffer
}

func fromStats(stats *stats, t time.Time, period time.Duration) *View {
	view := EmptyView()
	graph := make(map[string]Metric)
	for k, v := range stats.counters {
		perSec := float64(v) / period.Seconds()
		intoGraph(graph, k, perSec)
	}
	for k, v := range stats.gauges {
		intoGraph(graph, k, v)
	}
	for k, v := range stats.timers {
		mean := meanTimer(v)
		intoGraph(graph, k, mean)
	}
	for k, v := range stats.sets {
		uniques := len(v)
		perSec := float64(uniques) / period.Seconds()
		intoGraph(graph, k, perSec)
	}
	if len(graph) > 0 {
		view.Times = append(view.Times, t)
		view.Graphs = append(view.Graphs, graph)
	}
	return view
}

func intoGraph(graphs map[string]Metric, metricName string, value float64) {
	graphName, shortMetric := asGraph(metricName)
	if _, exists := graphs[graphName]; !exists {
		graphs[graphName] = make(map[string]float64)
	}
	graphs[graphName][shortMetric] = value
}

func asGraph(metricName string) (string, string) {
	last := strings.LastIndexByte(metricName, '.')
	if last != -1 {
		return metricName[:last], metricName[last+1:]
	}
	return "", metricName
}

func meanTimer(timers []uint32) float64 {
	n := len(timers)
	sum := uint32(0)
	for _, v := range timers {
		sum += v
	}
	return float64(sum) / float64(n)
}
