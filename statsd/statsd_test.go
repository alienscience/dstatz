package statsd

import (
	"net"
	"testing"
	"time"
)

func TestUdp(t *testing.T) {
	packet := `abc.def.g:100|c
abc.def.h:200|c
abc.def.i:300|c
abc.fgh.a:400|c
abc.fgh.b:500|c
abc.ijk.de:600|c
abc.ijk.ef:700|c`

	tickPeriod := time.Millisecond
	// Start a daemon
	statsd, err := NewDaemon(
		UDPAddress(":0"),
		DisableTCP(),
		TickPeriod(tickPeriod),
	)
	if err != nil {
		t.Fatal(err)
	}
	err = statsd.Run()
	if err != nil {
		t.Fatal(err)
	}
	defer statsd.Shutdown()

	// Subscribe to stats
	handle, stats := statsd.Subscribe("abc.", tickPeriod)
	defer statsd.Unsubscribe(handle)

	// Check the subscription
	finished := make(chan struct{})
	go func() {
		select {
		case view := <-stats:
			graph := make(map[string]Metric)
			graph["def"] = map[string]float64{"g": 0, "h": 0, "i": 0}
			graph["fgh"] = map[string]float64{"a": 0, "b": 0}
			graph["ijk"] = map[string]float64{"de": 0, "ef": 0}
			if len(view.Graphs) != 1 {
				t.Errorf("Expected %d graph got %d", 1, len(view.Graphs))
			} else if !sameKeys(graph, view.Graphs[0]) {
				t.Errorf("Expected same keys as %v got %v", graph, view.Graphs[0])
			}
		case <-time.After(time.Second):
			t.Error("Did not receive view from subscription")
		}
		close(finished)
	}()

	// Write a packet of stats
	addr := statsd.UDPAddress()
	conn, err := net.Dial("udp", addr.String())
	if err != nil {
		t.Fatal(err)
	}
	defer conn.Close()
	written, err := conn.Write([]byte(packet))
	if err != nil {
		t.Fatal(err)
	}
	if written != len(packet) {
		t.Fatalf("Written %d bytes but packet was %d bytes", written, len(packet))
	}
	<-finished
}

func sameKeys(expected map[string]Metric, actual map[string]Metric) bool {
	for ek, ev := range expected {
		av, ok := actual[ek]
		if !ok {
			return false
		}
		for em := range ev {
			_, ok = av[em]
			if !ok {
				return false
			}
		}
	}
	return true
}
