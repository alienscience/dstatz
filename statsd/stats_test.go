package statsd

import (
	"testing"
)

func TestIncCounter(t *testing.T) {
	s := newStats()
	s.incCounter("n", 1)
	if s.counters["n"] != 1 {
		t.Fatal("n != 1")
	}
	s.incCounter("n", 4)
	if s.counters["n"] != 5 {
		t.Fatal("n != 5")
	}
}

func TestIncSampledCounter(t *testing.T) {
	s := newStats()
	s.incSampledCounter("n", 1, 0.5)
	if s.counters["n"] != 2 {
		t.Fatal("n != 2")
	}
}

func TestUpdateGauge(t *testing.T) {
	s := newStats()
	s.updateGauge("g", 1.0)
	if s.gauges["g"] != 1.0 {
		t.Fatal("g != 1")
	}
	s.updateGauge("g", 1.1)
	if s.gauges["g"] != 1.1 {
		t.Fatal("g != 1.1")
	}
}

func TestIncGauge(t *testing.T) {
	s := newStats()
	s.updateGauge("g", 1.0)
	if s.gauges["g"] != 1.0 {
		t.Fatal("g != 1")
	}
	s.incGauge("g", 1.0)
	if s.gauges["g"] != 2.0 {
		t.Fatal("g != 2.0")
	}
}

func TestUpdateTimer(t *testing.T) {
	s := newStats()
	s.updateTimer("t", 100)
	if !(len(s.timers["t"]) == 1 && s.timers["t"][0] == 100) {
		t.Fatal("t != 100")
	}
	s.updateTimer("t", 200)
	if !(len(s.timers["t"]) == 2 && s.timers["t"][1] == 200) {
		t.Fatal("t != 100, 200")
	}
}

func TestAddToSet(t *testing.T) {
	s := newStats()
	s.addToSet("s", "s1")
	s.addToSet("s", "s2")
	s.addToSet("s", "s3")
	set := s.sets["s"]
	if !(set["s1"] && set["s2"] && set["s3"]) {
		t.Fatal("s != s1, s2, s3")
	}
}

func TestRestart(t *testing.T) {
	s := newStats()
	s.incCounter("n", 1)
	s.updateGauge("g", 1.0)
	s.updateTimer("t", 100)
	s.addToSet("s", "s1")
	s.restart()
	s.incCounter("n", 1)
	s.updateTimer("t", 100)
	s.addToSet("s", "s1")
	if s.counters["n"] != 1 {
		t.Fatal("n != 1")
	}
	if s.gauges["g"] != 1.0 {
		t.Fatal("g != 1")
	}
	if s.timers["t"][0] != 100 {
		t.Fatal("t != 100")
	}
	if !s.sets["s"]["s1"] {
		t.Fatal("s != s1")
	}
}

func TestUpdate(t *testing.T) {
	src := newStats()
	dest := newStats()
	src.incCounter("n", 1)
	dest.incCounter("n", 1)
	src.updateGauge("g", 2.0)
	dest.updateGauge("g", 1.0)
	src.updateTimer("t", 100)
	src.updateTimer("t", 200)
	dest.updateTimer("t", 100)
	src.addToSet("s", "s1")
	dest.addToSet("s", "s1")
	dest.update(src, "")
	if dest.counters["n"] != 2 {
		t.Fatal("n != 2")
	}
	if dest.gauges["g"] != 2.0 {
		t.Fatal("g != 2")
	}
	timers := dest.timers["t"]
	if !(len(timers) == 2 && timers[0] == 100 && timers[1] == 150) {
		t.Fatal("t != 150")
	}
	set := dest.sets["s"]
	if !(len(set) == 1 && set["s1"]) {
		t.Fatal("s != s1")
	}
}
