package statsd

import (
	"testing"
	"time"
)

func TestSubscribe(t *testing.T) {
	pubsub := newPubSub()
	id1, _ := pubsub.subscribe("", time.Minute)
	id2, _ := pubsub.subscribe("", time.Minute)
	if pubsub.numSubscriptions() != 2 {
		t.Fatal("Not enough subscriptions")
	}
	pubsub.unsubscribe(id1)
	pubsub.unsubscribe(id2)
	if pubsub.numSubscriptions() != 0 {
		t.Fatal("UnsubscribeError failed")
	}
}

func TestListen(t *testing.T) {
	pubsub := newPubSub()
	id, ch := pubsub.subscribe("", 0)
	defer pubsub.unsubscribe(id)
	running := make(chan bool)
	go func() {
		running <- true
		select {
		case <-ch:
			// Expected
		case <-time.After(10 * time.Second):
			t.Error("Did not receive view from subscription")
		}
		running <- false
	}()
	<-running
	stats := newStats()
	stats.counters["tst"] = 100
	pubsub.update(stats, time.Now())
	<-running
}

func TestFlush(t *testing.T) {
	pubsub := newPubSub()
	id, ch := pubsub.subscribe("", time.Hour)
	defer pubsub.unsubscribe(id)
	running := make(chan bool)
	go func() {
		running <- true
		select {
		case <-ch:
			// Expected
		case <-time.After(10 * time.Second):
			t.Error("Did not receive view from flush")
		}
		running <- false
	}()
	<-running
	stats := newStats()
	stats.counters["tst"] = 100
	pubsub.flush(stats, time.Now())
	<-running
}

func TestShutdown(t *testing.T) {
	pubsub := newPubSub()
	id, ch := pubsub.subscribe("", time.Hour)
	defer pubsub.unsubscribe(id)
	go pubsub.shutdown()
	select {
	case <-ch:
		// Expected
	case <-time.After(1 * time.Second):
		t.Error("Did not receive shutdown from pubsub")
	}
}
