package statsd

import (
	"bufio"
	"bytes"
	"fmt"
	"io"
	"strconv"
)

type token int

const (
	tokColon = iota
	tokPipe
	tokEndl
	tokRelative
	tokID
	tokNumber
	tokRate
	tokEOF
	tokUnknown
)

var eof = rune(0)

func isID(ch rune) bool {
	return ch != '\n' && ch != '|' && ch != ':' && ch != '@'
}

func isNumber(ch rune) bool {
	return (ch >= '0' && ch <= '9') || ch == '.'
}

func isRelative(ch rune) bool {
	return ch == '-' || ch == '+'
}

type scanner struct {
	r *bufio.Reader
}

func newScanner(r io.Reader) *scanner {
	return &scanner{r: bufio.NewReader(r)}
}

func (s *scanner) read() rune {
	ch, _, err := s.r.ReadRune()
	if err != nil {
		return eof
	}
	return ch
}

func (s *scanner) unread() {
	_ = s.r.UnreadRune()
}

func (s *scanner) scan() (token, string) {
	ch := s.read()
	switch {
	case ch == eof:
		return tokEOF, ""
	case ch == ':':
		return tokColon, string(ch)
	case ch == '|':
		return tokPipe, string(ch)
	case ch == '\n':
		return tokEndl, string(ch)
	case ch == '@':
		return s.scanNumber(tokRate)
	case isRelative(ch):
		return tokRelative, string(ch)
	case isNumber(ch):
		s.unread()
		return s.scanNumber(tokNumber)
	case isID(ch):
		s.unread()
		return s.scanID()
	default:
		return tokUnknown, string(ch)
	}
}

func (s *scanner) scanID() (token, string) {
	var buf bytes.Buffer
	for {
		ch := s.read()
		if ch == eof {
			break
		} else if !isID(ch) {
			s.unread()
			break
		} else {
			buf.WriteRune(ch)
		}
	}
	return tokID, buf.String()
}

func (s *scanner) scanNumber(asTok token) (token, string) {
	var buf bytes.Buffer
	for {
		ch := s.read()
		if ch == eof {
			break
		} else if !isNumber(ch) {
			s.unread()
			break
		} else {
			buf.WriteRune(ch)
		}
	}
	return asTok, buf.String()

}

type parser struct {
	s *scanner
}

func newParser(r io.Reader) *parser {
	return &parser{s: newScanner(r)}
}

// Returns (stat, isEof, error)
func (p *parser) parseStat() (*stat, bool, error) {
	stat := &stat{sampleRate: 1.0}
	firstTok, firstLit := p.s.scan()
	for firstTok == tokEndl {
		firstTok, firstLit = p.s.scan()
	}
	if firstTok == tokID {
		stat.bucket = firstLit
	} else if firstTok == tokEOF {
		return nil, true, nil
	} else {
		return nil, false, fmt.Errorf("Expected id got %q", firstLit)
	}
	if tok, lit := p.s.scan(); tok != tokColon {
		return nil, false, fmt.Errorf("Expected ':' got %q", lit)
	}
	valTok, valLit := p.s.scan()
	if !(valTok == tokRelative || valTok == tokNumber || valTok == tokID) {
		return nil, false, fmt.Errorf("Expected value got %q", valLit)
	}
	isRelative := false
	if valTok == tokRelative {
		isRelative = true
		isRelativeLit := valLit
		valTok, valLit = p.s.scan()
		if valTok != tokNumber {
			return nil, false, fmt.Errorf("Expected number got %q", valLit)
		}
		valLit = isRelativeLit + valLit
	}
	if tok, lit := p.s.scan(); tok != tokPipe {
		return nil, false, fmt.Errorf("Expected '|' got %q", lit)
	}

	tok, lit := p.s.scan()
	if tok != tokID {
		return nil, false, fmt.Errorf("Expected id got %q", lit)
	}
	typ, err := toStatsType(lit)
	if err != nil {
		return nil, false, err
	}
	stat.typeOfStat = typ
	switch typ {
	case counterType:
		intVal, err := strconv.ParseInt(valLit, 10, 64)
		if err != nil {
			return nil, false, err
		}
		stat.val.asInt = intVal
	case gaugeType:
		floatVal, err := strconv.ParseFloat(valLit, 64)
		if err != nil {
			return nil, false, err
		}
		stat.val.asFloat = floatVal
		stat.val.isRelative = isRelative
	case timerType:
		intVal, err := strconv.ParseInt(valLit, 10, 64)
		if err != nil {
			return nil, false, err
		}
		stat.val.asInt = intVal
	case setType:
		stat.val.asString = valLit
	}
	trailingTok, trailingLit := p.s.scan()
	if trailingTok == tokRate {
		rate, err := strconv.ParseFloat(trailingLit, 32)
		if err != nil {
			return nil, false, err
		}
		stat.sampleRate = float32(rate)
	}
	return stat, false, nil
}

func toStatsType(s string) (statsType, error) {
	switch s {
	case "c":
		return counterType, nil
	case "g":
		return gaugeType, nil
	case "ms":
		return timerType, nil
	case "s":
		return setType, nil
	default:
		return unknownType, fmt.Errorf("Unknown stats type %q", s)
	}
}
