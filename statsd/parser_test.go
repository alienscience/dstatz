package statsd

import (
	"bytes"
	"testing"
)

func TestParser(t *testing.T) {
	tests := []struct {
		in       string
		expected []stat
	}{
		// Single line
		{
			in: "some.stat.name:1|c",
			expected: []stat{
				{bucket: "some.stat.name", typeOfStat: counterType, val: statsValue{asInt: 1}, sampleRate: 1.0},
			},
		},
		// Multiline
		{
			in: "some.counter:1|c\nsome.timer:320|ms\nsome.gauge:333|g\nsome.set:v1|s\n",
			expected: []stat{
				{bucket: "some.counter", typeOfStat: counterType, val: statsValue{asInt: 1}, sampleRate: 1.0},
				{bucket: "some.timer", typeOfStat: timerType, val: statsValue{asInt: 320}, sampleRate: 1.0},
				{bucket: "some.gauge", typeOfStat: gaugeType, val: statsValue{asFloat: 333.0}, sampleRate: 1.0},
				{bucket: "some.set", typeOfStat: setType, val: statsValue{asString: "v1"}, sampleRate: 1.0},
			},
		},
		// Sample rate with following line
		{
			in: "some.stat.name:1|c@0.1\nsome.gauge:333|g",
			expected: []stat{
				{bucket: "some.stat.name", typeOfStat: counterType, val: statsValue{asInt: 1}, sampleRate: 0.1},
				{bucket: "some.gauge", typeOfStat: gaugeType, val: statsValue{asFloat: 333.0}, sampleRate: 1.0},
			},
		},
	}
	for _, test := range tests {
		p := newParser(bytes.NewBufferString(test.in))
		for _, e := range test.expected {
			s, isEOF, err := p.parseStat()
			if err != nil {
				t.Fatal("Error parsing ", test.in, err)
			}
			if isEOF {
				t.Fatal("Eof parsing ", test.in)
			}
			if *s != e {
				t.Error("Unexpected parsed stat", s, e)
			}
		}
		s, isEOF, err := p.parseStat()
		if err != nil {
			t.Fatal("Error parsing at EOF", test.in, err)
		}
		if !isEOF {
			t.Fatal("Expected Eof got stat ", test.in, s)
		}
	}
}
