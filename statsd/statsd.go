package statsd

import (
	"bytes"
	"errors"
	"io"
	"log"
	"net"
	"time"
)

const (
	// DefaultStatsQueueSize is the size of the queue between the network listeners
	// and the statsd event loop.
	DefaultStatsQueueSize = 32
	// MaxUDPPacketSize is the maximum UDP packet size
	MaxUDPPacketSize = 1472
	// DefaultUDPAddress is the default UDP address to listen on
	DefaultUDPAddress = ":8125"
	// DefaultTCPAddress is the default TCP address to listen on
	DefaultTCPAddress = ":8126"
	// DefaultInternalPrefix is the default prefix for internal statistics
	DefaultInternalPrefix = "statsd."
)

const (
	counterType = iota
	gaugeType
	timerType
	setType
	unknownType
)

var (
	// DefaultTick is the default period between aggregating statistics
	DefaultTick = time.Second
)

type statsType int

type statsValue struct {
	asInt      int64
	asFloat    float64
	asString   string
	isRelative bool
}

type stat struct {
	bucket     string
	typeOfStat statsType
	val        statsValue
	sampleRate float32
}

type listeners struct {
	udpEnabled       bool
	tcpEnabled       bool
	udpAddress       string
	tcpAddress       string
	actualUDPAddress net.Addr
	actualTCPAddress net.Addr
}

// Daemon encapsulates a statistics daemon
type Daemon struct {
	shutdown       chan struct{}
	done           chan struct{}
	tickPeriod     time.Duration
	queueSize      int
	internalPrefix string
	listeners      listeners
	in             chan *stat
	stats          *stats
	pubsub         *pubSub
}

// NewDaemon creates a statistics daemon
func NewDaemon(options ...func(*Daemon) error) (*Daemon, error) {
	daemon := &Daemon{
		shutdown:       make(chan struct{}),
		done:           make(chan struct{}),
		tickPeriod:     DefaultTick,
		queueSize:      DefaultStatsQueueSize,
		internalPrefix: DefaultInternalPrefix,
		listeners: listeners{
			udpEnabled: true,
			tcpEnabled: true,
			udpAddress: DefaultUDPAddress,
			tcpAddress: DefaultTCPAddress,
		},
		stats:  newStats(),
		pubsub: newPubSub(),
	}

	for _, opt := range options {
		if err := opt(daemon); err != nil {
			return nil, err
		}
	}
	return daemon, nil
}

// TickPeriod sets the period between statistics aggregations
func TickPeriod(period time.Duration) func(*Daemon) error {
	return func(d *Daemon) error {
		d.tickPeriod = period
		return nil
	}
}

// QueueSize sets the size of the queue between the network listeners and
// the statsd event loop.
func QueueSize(size int) func(*Daemon) error {
	return func(d *Daemon) error {
		if size < 1 {
			return errors.New("Daemon queue size must be at least 1")
		}
		d.queueSize = size
		return nil
	}
}

// UDPAddress sets the UDP address to listen on
func UDPAddress(address string) func(*Daemon) error {
	return func(d *Daemon) error {
		d.listeners.udpAddress = address
		return nil
	}
}

// TCPAddress sets the TCP address to listen on
func TCPAddress(address string) func(*Daemon) error {
	return func(d *Daemon) error {
		d.listeners.tcpAddress = address
		return nil
	}
}

// DisableUDP disables statistics over UDP
func DisableUDP() func(*Daemon) error {
	return func(d *Daemon) error {
		d.listeners.udpEnabled = false
		return nil
	}
}

// DisableTCP disables statistics over TCP
func DisableTCP() func(*Daemon) error {
	return func(d *Daemon) error {
		d.listeners.tcpEnabled = false
		return nil
	}
}

// InternalStats sets the prefix of the internal statistics
func InternalStats(prefix string) func(*Daemon) error {
	return func(d *Daemon) error {
		d.internalPrefix = prefix
		return nil
	}
}

// Run runs the statistics daemon
func (d *Daemon) Run() error {
	d.in = make(chan *stat, d.queueSize)
	if err := d.udpListener(); err != nil {
		return err
	}
	if err := d.tcpListener(); err != nil {
		return err
	}
	go d.eventLoop()
	return nil
}

// Shutdown stops the statistics daemon
func (d *Daemon) Shutdown() {
	close(d.shutdown)
	<-d.done
}

// UDPAddress returns the UDP address that the daemon is listening on
func (d *Daemon) UDPAddress() net.Addr {
	return d.listeners.actualUDPAddress
}

// TCPAddress returns the TCP address that the daemon is listening on
func (d *Daemon) TCPAddress() net.Addr {
	return d.listeners.actualTCPAddress
}

// Subscribe obtains a channel which will receive statistics every given period.
// The prefix filters statistics but can be an empty string to receive all statistics.
func (d *Daemon) Subscribe(prefix string, period time.Duration) (uint32, chan *View) {
	return d.pubsub.subscribe(prefix, period)
}

// Unsubscribe cancels a subscription obtained with Subscribe
func (d *Daemon) Unsubscribe(id uint32) {
	d.pubsub.unsubscribe(id)
}

// IncCounter increments a counter
func (d *Daemon) IncCounter(bucket string, value int64) {
	select {
	case d.in <- &stat{
		bucket:     bucket,
		typeOfStat: counterType,
		val: statsValue{
			asInt: value,
		},
		sampleRate: 1.0,
	}:
	default:
	}
}

// UpdateGauge sets the value of a gauge
func (d *Daemon) UpdateGauge(bucket string, value float64) {
	d.in <- &stat{
		bucket:     bucket,
		typeOfStat: gaugeType,
		val: statsValue{
			asFloat: value,
		},
		sampleRate: 1.0,
	}
}

// AddTiming add a timer value
func (d *Daemon) AddTiming(bucket string, value int64) {
	d.in <- &stat{
		bucket:     bucket,
		typeOfStat: timerType,
		val: statsValue{
			asInt: value,
		},
		sampleRate: 1.0,
	}
}

// AddSetElement adds an element to a set
func (d *Daemon) AddSetElement(bucket string, value string) {
	d.in <- &stat{
		bucket:     bucket,
		typeOfStat: setType,
		val: statsValue{
			asString: value,
		},
		sampleRate: 1.0,
	}
}

func (d *Daemon) udpListener() error {
	if !d.listeners.udpEnabled {
		return nil
	}
	addr, _ := net.ResolveUDPAddr("udp", d.listeners.udpAddress)
	listener, err := net.ListenUDP("udp", addr)
	if err != nil {
		return err
	}
	d.listeners.actualUDPAddress = listener.LocalAddr()
	go d.udpPackets(listener)
	return nil
}

func (d *Daemon) udpPackets(listener *net.UDPConn) {
	defer listener.Close()
	for {
		// Read packet
		buffer := make([]byte, MaxUDPPacketSize)
		_, err := listener.Read(buffer)
		if err != nil {
			log.Print(err)
			return
		}
		// Parse packet
		b := bytes.NewBuffer(buffer)
		parser := newParser(b)
		for {
			stat, isEOF, err := parser.parseStat()
			if err != nil {
				log.Print(err)
				d.IncCounter(d.internalPrefix+"err.udp", 1)
				break
			}
			if isEOF {
				break
			}
			d.in <- stat
		}
		d.IncCounter(d.internalPrefix+"udp.packet", 1)
	}
}

func (d *Daemon) tcpListener() error {
	if !d.listeners.tcpEnabled {
		return nil
	}
	addr, err := net.ResolveTCPAddr("tcp", d.listeners.tcpAddress)
	if err != nil {
		return err
	}
	listener, err := net.ListenTCP("tcp", addr)
	if err != nil {
		return err
	}
	d.listeners.actualTCPAddress = listener.Addr()
	go d.tcpAccept(listener)
	return nil
}

func (d *Daemon) tcpAccept(listener *net.TCPListener) {
	defer listener.Close()
	for {
		conn, err := listener.AcceptTCP()
		if err != nil {
			log.Printf("tcplistener %s", err)
			continue
		}
		go d.tcpClient(conn)
	}
}

func (d *Daemon) tcpClient(conn io.ReadCloser) {
	d.IncCounter(d.internalPrefix+"tcp.client", 1)
	defer conn.Close()
	parser := newParser(conn)
	for {
		stat, isEOF, err := parser.parseStat()
		if err != nil {
			d.IncCounter(d.internalPrefix+"err.tcp", 1)
			log.Print(err)
			return
		}
		if isEOF {
			return
		}
		d.in <- stat
	}
}

func (d *Daemon) eventLoop() {
	defer close(d.done)
	ticker := time.NewTicker(d.tickPeriod)
	defer ticker.Stop()
	for {
		select {
		case <-d.shutdown:
			d.flush()
			d.pubsub.shutdown()
			return
		case <-ticker.C:
			d.update()
		case s := <-d.in:
			d.statHandler(s)
		}
	}
}

func (d *Daemon) flush() {
	now := time.Now().UTC()
	d.pubsub.flush(d.stats, now)
	d.stats.restart()
}

func (d *Daemon) update() {
	now := time.Now().UTC()
	d.pubsub.update(d.stats, now)
	d.stats.restart()
}

func (d *Daemon) statHandler(s *stat) {
	switch s.typeOfStat {
	case counterType:
		if s.sampleRate < 1.0 {
			d.stats.incSampledCounter(s.bucket, s.val.asInt, float64(s.sampleRate))
		} else {
			d.stats.incCounter(s.bucket, s.val.asInt)
		}
	case gaugeType:
		if s.val.isRelative {
			d.stats.incGauge(s.bucket, s.val.asFloat)
		} else {
			d.stats.updateGauge(s.bucket, s.val.asFloat)
		}
	case timerType:
		d.stats.updateTimer(s.bucket, s.val.asInt)
	case setType:
		d.stats.addToSet(s.bucket, s.val.asString)
	}
}
