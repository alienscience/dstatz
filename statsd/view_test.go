package statsd

import (
	"reflect"
	"testing"
	"time"
)

func TestFromStats(t *testing.T) {
	now := time.Now().UTC()
	stats := newStats()
	stats.counters["c.c"] = 100
	stats.gauges["g.g"] = 100
	stats.timers["t.t"] = []uint32{100, 200}
	stats.sets["s.s"] = map[string]bool{"s1": true, "s2": true}
	view := fromStats(stats, now, 10*time.Second)
	expected := EmptyView()
	graph := make(map[string]Metric)
	graph["c"] = map[string]float64{"c": 10.0}
	graph["g"] = map[string]float64{"g": 100.0}
	graph["t"] = map[string]float64{"t": 150.0}
	graph["s"] = map[string]float64{"s": 0.2}
	expected.Times = append(expected.Times, now)
	expected.Graphs = append(expected.Graphs, graph)
	if !reflect.DeepEqual(expected, view) {
		t.Error("Unexpected view ", expected, view)
	}
}

func TestWithoutGraph(t *testing.T) {
	now := time.Now().UTC()
	stats := newStats()
	stats.counters["c"] = 100
	stats.gauges["g"] = 100
	stats.timers["t"] = []uint32{100, 200}
	stats.sets["s"] = map[string]bool{"s1": true, "s2": true}
	view := fromStats(stats, now, 10*time.Second)
	expected := EmptyView()
	graph := make(map[string]Metric)
	graph[""] = map[string]float64{
		"c": 10.0,
		"g": 100.0,
		"t": 150.0,
		"s": 0.2,
	}
	expected.Times = append(expected.Times, now)
	expected.Graphs = append(expected.Graphs, graph)
	if !reflect.DeepEqual(expected, view) {
		t.Error("Unexpected view ", expected, view)
	}
}
