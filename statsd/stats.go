package statsd

import (
	"strings"
)

type stats struct {
	counters map[string]int64
	gauges   map[string]float64
	timers   map[string][]uint32
	sets     map[string]set
}

type set map[string]bool

func newStats() *stats {
	return &stats{
		counters: make(map[string]int64),
		gauges:   make(map[string]float64),
		timers:   make(map[string][]uint32),
		sets:     make(map[string]set),
	}
}

func (s *stats) restart() {
	s.counters = make(map[string]int64)
	// Gauges not reset
	s.timers = make(map[string][]uint32)
	s.sets = make(map[string]set)
}

func (s *stats) incCounter(name string, count int64) {
	c, _ := s.counters[name]
	s.counters[name] = c + count
}

func (s *stats) incSampledCounter(name string, count int64, sampling float64) {
	c, _ := s.counters[name]
	s.counters[name] = c + int64(float64(count)/sampling)
}

func (s *stats) updateGauge(name string, value float64) {
	s.gauges[name] = value
}

func (s *stats) incGauge(name string, value float64) {
	g, _ := s.gauges[name]
	g += value
	s.gauges[name] = g
}

func (s *stats) updateTimer(name string, ms int64) {
	t, ok := s.timers[name]
	if !ok {
		t = make([]uint32, 0, 4)
	}
	s.timers[name] = append(t, uint32(ms))
}

func (s *stats) addToSet(name string, entry string) {
	_, exists := s.sets[name]
	if !exists {
		s.sets[name] = make(map[string]bool)
	}
	s.sets[name][entry] = true
}

func (s *stats) update(other *stats, prefix string) {
	for k, v := range other.counters {
		if strings.HasPrefix(k, prefix) {
			name := strings.TrimPrefix(k, prefix)
			s.counters[name] += v
		}
	}
	for k, v := range other.gauges {
		if strings.HasPrefix(k, prefix) {
			name := strings.TrimPrefix(k, prefix)
			s.gauges[name] = v
		}
	}
	for k, v := range other.timers {
		if strings.HasPrefix(k, prefix) {
			name := strings.TrimPrefix(k, prefix)
			s.timers[name] = append(s.timers[name], timerAve(v))
		}
	}
	for k, v := range other.sets {
		if strings.HasPrefix(k, prefix) {
			name := strings.TrimPrefix(k, prefix)
			_, ok := s.sets[name]
			if !ok {
				s.sets[name] = make(map[string]bool)
			}
			for e := range v {
				s.sets[name][e] = true
			}
		}
	}
}

func timerAve(ms []uint32) uint32 {
	sum := uint32(0)
	n := len(ms)
	for _, v := range ms {
		sum += v
	}
	return sum / uint32(n)
}
